import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'postgresql://postgres:1234@localhost/library'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    UPLOAD_FOLDER='app/uploads'
    ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif','json'])
    # es = Elasticsearch('https://8f31b9b7c8974f87baa38f970ccfde37.ap-southeast-1.aws.found.io:9243/', use_ssl=True, ca_certs=certifi.where(),http_auth=('elastic','N7Wv9mVPskcEj5ftjwauiYND'))
    # ELASTICSEARCH_URL=('https://4d286a6adb2b4a1eaee798c79deeb123.ap-southeast-1.aws.found.io:9243/')
    # ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
    CELERY_BROKER_URL= 'redis://localhost:6379/0'
    CELERY_RESULT_BACKEND= 'redis://localhost:6379/0'
