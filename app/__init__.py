
import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from config import Config
from werkzeug.utils import secure_filename
from flask_bootstrap import Bootstrap
# from elasticsearch import Elasticsearch
# import certifi
from celery import Celery

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'login'
bootstrap = Bootstrap(app)
# app.elasticsearch = Elasticsearch('https://8f31b9b7c8974f87baa38f970ccfde37.ap-southeast-1.aws.found.io:9243/', use_ssl=True, ca_certs=certifi.where(),http_auth=('elastic','N7Wv9mVPskcEj5ftjwauiYND'))
from app import routes, models
